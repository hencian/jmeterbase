# Use Java 11 JDK Oracle Linux
FROM openjdk:11-jdk-oracle

MAINTAINER Horie

# JMeter version
ARG	JMETER_VERSION="5.1.1"
ARG	JMETER_PLUGIN_MANAGER_VERSION="1.3"

# Set JMeter related environment variables
ENV	JMETER_HOME /opt/apache-jmeter-${JMETER_VERSION}
ENV	JMETER_BIN ${JMETER_HOME}/bin
ENV	JMETER_DOWNLOAD_URL https://archive.apache.org/dist/jmeter/binaries/apache-jmeter-${JMETER_VERSION}.tgz
#ENV	JMETER_DOWNLOAD_URL https://cl.ly/b7a70fa644bf/custom_jmeter.tgz

# Set default values for allocation of system resources by JMeter
ENV	Xms 256m
ENV	Xmx 512m
ENV	MaxMetaspaceSize 1024m

# Change timezone to local time - TBD
ARG	TZ="Europe/Bucharest"

# Install jmeter
RUN	yum -y install curl \
	&& mkdir -p /tmp/dependencies \
	&& curl -L --silent ${JMETER_DOWNLOAD_URL} >  /tmp/dependencies/apache-jmeter-${JMETER_VERSION}.tgz \
	&& mkdir -p /opt \
	&& tar -xzf /tmp/dependencies/apache-jmeter-${JMETER_VERSION}.tgz -C /opt \
	&& rm -rf /tmp/dependencies \
	&& yum -y upgrade \
	&& yum -y install wget \
	&& yum -y install unzip

#Install wget
#RUN apt-get update && apt-get install -y apt-transport-https wget apt-utils

#Install Jmeter plugin manager and jmeter plugins
RUN wget -P ${JMETER_HOME}/lib/ext --trust-server-names https://jmeter-plugins.org/get/
RUN mv ${JMETER_HOME}/lib/ext/remotecontent?filepath=kg%2Fapc%2Fjmeter-plugins-manager%2F1.3%2Fjmeter-plugins-manager-${JMETER_PLUGIN_MANAGER_VERSION}.jar ${JMETER_HOME}/lib/ext/jmeter-plugins-manager-${JMETER_PLUGIN_MANAGER_VERSION}.jar
RUN wget -O ${JMETER_HOME}/lib/cmdrunner-2.2.jar  "http://search.maven.org/remotecontent?filepath=kg/apc/cmdrunner/2.2/cmdrunner-2.2.jar"
RUN java -cp ${JMETER_HOME}/lib/ext/jmeter-plugins-manager-${JMETER_PLUGIN_MANAGER_VERSION}.jar org.jmeterplugins.repository.PluginManagerCMDInstaller
RUN ${JMETER_HOME}/bin/PluginsManagerCMD.sh install \
	bzm-parallel=0.9,\
	jpgc-redis=0.3
RUN wget -P /home/ --trust-server-names "https://jmeter-plugins.org/files/packages/jpgc-redis-0.3.zip"
RUN unzip -o /home/jpgc-redis-0.3.zip -d ${JMETER_HOME}/


# Set JMeter home
ENV PATH $PATH:$JMETER_BIN

# copy our entrypoint
COPY entrypoint.sh /

RUN chmod +x ./entrypoint.sh

# Run command to allocate the default system resources to JMeter at 'docker run'
ENTRYPOINT	["/entrypoint.sh"]
